<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    protected $table = 'articles';
    protected $primaryKey = 'id';

    public static function getAllSortWithPaginate($count)
    {
        return self::orderBy('created_at', 'DESC')->paginate($count);
    }
    public static function getById(int $id) : Article|null
    {
        return self::where('id', $id)->first();
    }
    public static function create(array $params) : int
    {
        $article = new Article();

        $article->created_at = now();
        $article->updated_at = now();
        $article->title = $params['title'];
        $article->image = $params['image'];
        $article->text = $params['text'];
        $article->publication_status = $params['publication_status'];

        $article->save();

        return  $article->id;
     }
    public static function updateById(int $id, $params) : void
     {
         $article = self::getById($id);

         $article->updated_at = now();
         $article->title = $params['title'];
         $article->text = $params['text'];
         $article->publication_status = $params['publication_status'];

         if (isset($params['image'])) {
             $article->image = $params['image'];
         }

         $article->save();
     }

    public static function deleteById(int $id)
    {
        $article = self::getById($id);
        $article->delete();
    }

    public static function getImgById(int $id)
     {
         return self::where('id', $id)->first()->image;
     }
}
