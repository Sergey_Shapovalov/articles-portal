<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ImgService
{
    public function saveImg(string $directory, UploadedFile $img): string
    {
        $filePath = Storage::disk('public')->putFile($directory , $img);
        return '/storage/' . $filePath;
    }
    public function deleteImg(string $imgPath): void
    {
        $imgPath =  str_replace('/storage/', '', $imgPath);
        Storage::disk('public')->delete($imgPath);
    }
    public function updateImg(string $directory, string $oldImgPath, UploadedFile $img): string
    {
        $storagePath = str_replace('/storage/', '', $oldImgPath);
        $this->deleteImg($storagePath);

        return $this->saveImg($directory, $img);
    }
}
