<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Services\ImgService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class ArticleController extends Controller
{
    public function __construct(
        protected ImgService $imgService
    ) {}

    public function index(Request $request)
    {
        $articles = Article::getAllSortWithPaginate(5);

        return view('articles.index', compact('articles'));
    }
    public function show(Request $request, int $id)
    {
        $article = Article::getById($id);

        return view('articles.article', compact('article'));
    }
    public function create(Request $request)
    {
        return view('articles.create');
    }

    public function edit(Request $request, int $id)
    {
        $article = Article::getById($id);

        return view('articles.edit', compact('article'));
    }
    public function store(Request $request)
    {
        $params = $request->validate([
            'title' => 'required|max:255',
            'image' => 'required|file|mimes:jpg,png,jpeg',
            'text' => 'required',
            'publication_status' => 'nullable'
        ]);

        if(!array_key_exists('publication_status', $params)) {
            $params['publication_status'] = false;
        } else {
            $params['publication_status'] = true;
        }

        $params['image'] =  $this->imgService->saveImg('articles', $params['image']);

        Article::create($params);

        return redirect()->route('article.index');
    }
    public function update(Request $request, int $id)
    {
        $params = $request->validate([
            'title' => 'required|max:255',
            'image' => 'nullable|file|mimes:jpg,png,jpeg',
            'text' => 'required',
            'publication_status' => 'nullable'
        ]);

        $params['publication_status'] =  $params['publication_status'] ?? false;

        $articleImgPath = Article::getImgById($id);

        if(array_key_exists('image', $params)) {
            $params['image'] = $this->imgService->updateImg('articles', $articleImgPath, $params['image']);
        }

        Article::updateById($id, $params);

        return redirect()->route('article.show', ['id' => $id]);
    }
    public function delete(Request $request, int $id)
    {
        $article = Article::getById($id);

        Article::deleteById($article->id);

        $this->imgService->deleteImg($article->image);

        return redirect()->route('article.index');
    }
}

