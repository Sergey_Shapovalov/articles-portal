@extends("layouts.auth")

@section("content")
    <div style="text-align: center; margin-bottom: 20px">
        <h2>Регистрация</h2>
    </div>
    <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
        <form >
            <div class="mb-3">
                <label for="registrationName" class="form-label">Имя</label>
                <input type="text" class="form-control" id="registrationName" required>
            </div>
            <div class="mb-3">
                <label for="registrationEmail" class="form-label">Почта</label>
                <input type="email" class="form-control" id="registrationEmail" required>
            </div>
            <div class="mb-3">
                <label for="registrationPassword" class="form-label">Пароль</label>
                <input type="password" class="form-control" id="registrationPassword" required>
            </div>
            <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
        </form>
    </div>
@endsection
