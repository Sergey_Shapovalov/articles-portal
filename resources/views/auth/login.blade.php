@extends("layouts.auth")

@section("content")
    <div style="text-align: center; margin-bottom: 20px">
        <h2>Авторизация</h2>
    </div>
    <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
        <form >
            <div class="mb-3">
                <label for="loginEmail" class="form-label">Почта</label>
                <input type="email" class="form-control" id="loginEmail" required>
            </div>
            <div class="mb-3">
                <label for="loginPassword" class="form-label">Пароль</label>
                <input type="password" class="form-control" id="loginPassword" required>
            </div>
            <button type="submit" class="btn btn-primary">Войти</button>
        </form>
    </div>
@endsection
