@extends('layouts.app')

@section("content")
    <div style="display: flex; flex-direction: row-reverse">
        <div style="float: right; display: flex; margin-left: 20px; margin-top: 60px; flex-direction: column">
            <div>
                <a href="/article/{{$article->id}}/edit">
                    <button class="btn btn-primary" style="float: right; width: 200px">Редактировать</button>
                </a>
            </div>
            <div>
                <form method="post" action="{{route('article.delete', ['id' => $article->id])}}">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger" style="float: right; width: 200px; margin-top: 20px" >Удалить</button>
                </form>
            </div>
        </div>
        <div class="card text-center article-container" style="width: 80%; margin: 60px auto 0 auto; padding: 60px">
            <div class="card-title mb-4 ">
                <h2 class="card-text">{{$article->title}}</h2>
            </div>
            <img src="{{$article->image}}" class="card-img-top" alt="..." height="100%">
            <div class="card-body mt-4">
                <div class="card-text" style="text-align: left">
                    {!! $article->text !!}
                </div>
            </div>
        </div>
    </div>
@endsection
