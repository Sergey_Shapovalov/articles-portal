@extends('layouts.app')

@section("content")
    <div style="text-align: center; margin: 20px 0 20px 0">
        <h2>Редактирование статьи</h2>
    </div>
    <pre>
    </pre>
    <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
        <form method="post" action="{{route('article.update', ['id' => $article->id])}}" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="mb-3">
                <label for="articleTitle" class="form-label">Название</label>
                <input name="title"
                       type="text"
                       value="{{old('title') ?? $article->title}}"
                       class="form-control @error('title') is-invalid @enderror"
                       id="articleTitle"
                       required >
                @error('title')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="articleImg" class="form-label">Текущее изображение</label>
                <img src="{{$article->image}}" style="max-height: 400px">
            </div>
            <div class="mb-3">
                <label for="articleImg" class="form-label">Новое изображение</label>
                <input
                    type="file"
                    value="{{old('image') ?? $article->image}}"
                    name="image"
                    class="form-control  @error('image') is-invalid @enderror"
                    id="articleImg"
                    accept=".jpg,.jpeg,.png">
                @error('image')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="article-text" class="form-label">Текст</label>
                <textarea name="text" id="article-text" class="@error('text') is-invalid @enderror">{!! old('text') ?? $article->text !!}</textarea>
                @error('text')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>
            <div class="form-check mb-3">
                <input name="publication_status" class="form-check-input" type="checkbox" value="true" id="publishStatusChecked" checked>
                <label class="form-check-label" for="publishStatusChecked">
                    Опубликовать статью после создания
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
    </div>
@endsection
