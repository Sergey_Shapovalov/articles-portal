@extends('layouts.app')

@section("content")
   <div class="articles-container">
       <div style="height: 100px">

       </div>
       @foreach($articles as $article)
           <a href="/article/{{$article->id}}">
               <div class="card text-center article-container">
                   <div class="card-title mb-4">
                       <h2 class="card-text">{{$article->title}}</h2>
                   </div>
                   <img src="{{$article->image}}" class="card-img-top" height="100%">
                   <div class="card-body mt-4">
                       <div class="card-text">
                           {!! $article->text !!}
                       </div>
                   </div>
               </div>
           </a>
       @endforeach
       <div class="pagination-container">
           {{ $articles->links() }}
       </div>
   </div>
@endsection

@section("styles")
    <style>
        .articles-container {
            cursor: pointer;
        }

        .article-container {
            width: 70%;
            margin: 20px auto 30px auto;
            border-radius: 20px;
        }

        .card-text {
            overflow: hidden;
            max-height: 10ch;
        }

        .card-title {
            margin: 20px 10px 0 10px;
        }

        .pagination-container {

            width: 40%;
            margin: auto;
        }
        .pagination-container nav {
            width: 20%;
            margin: auto;
        }
        a {
            text-decoration: none;
        }
    </style>
@endsection
