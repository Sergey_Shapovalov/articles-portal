@extends('layouts.app')

@section("content")
    <div style="text-align: center; margin: 20px 0 20px 0">
        <h2>Новая статья</h2>
    </div>
    <div class="shadow p-3 mb-5 bg-body-tertiary rounded">
        <form method="post" action="{{route('article.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="articleTitle" class="form-label">Название</label>
                <input type="text" name="title" class="form-control" id="articleTitle"  maxlength="255" required>
            </div>
            <div class="mb-3">
                <label for="articleImg" class="form-label">Изображение</label>
                <input type="file" name="image" class="form-control" id="articleImg"  accept=".jpg,.jpeg,.png" required>
            </div>
            <div class="mb-3">
                <label for="article-text" class="form-label">Текст</label>
                <textarea name="text" id="article-text"></textarea>
            </div>
            <div class="form-check mb-3">
                <input  name="publication_status" class="form-check-input" type="checkbox" value="" id="publishStatusChecked" checked>
                <label class="form-check-label" for="publishStatusChecked">
                    Опубликовать статью после создания
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Создать</button>
        </form>
    </div>
@endsection
