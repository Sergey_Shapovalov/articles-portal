<div>
    <nav class="navbar bg-body-tertiary">
        <div class="container-fluid">
            <div style="display: flex">
                <div class="nav-item">
                    <a href="/" class="navbar-brand">Все статьи</a>
                </div>
                <div class="nav-item" style="margin-left: 30px">
                    <a href="/article/create">
                        <button class="btn btn-outline-success">Добавить статью</button>
                    </a>
                </div>
            </div>
            <div class="d-flex" role="search">
{{--                <button class="btn btn-outline-primary">Зарегистрироваться</button>--}}
{{--                <button class="btn btn-outline-primary">Войти</button>--}}
                <div class="nav-item" style="margin: auto 40px auto 20px">
                    Шаповалов Сергей
                </div>
                <a><button class="btn btn-outline-primary">Выйти</button></a>
            </div>
        </div>
    </nav>
</div>
