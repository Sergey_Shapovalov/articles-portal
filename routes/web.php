<?php

use App\Http\Controllers\ArticleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [ArticleController::class, 'index'])->name('article.index');

Route::prefix("article")->group(function () {
    Route::get('/{id}', [ArticleController::class, 'show'])->whereNumber('id')->name('article.show');
    Route::get('/create', [ArticleController::class, 'create'])->name('article.create');
    Route::get('/{id}/edit', [ArticleController::class, 'edit'])->whereNumber('id')->name('article.edit');
    Route::post('/', [ArticleController::class, 'store'])->name('article.store');
    Route::put('/{id}/update', [ArticleController::class, 'update'])->whereNumber('id')->name('article.update');
    Route::delete('/{id}', [ArticleController::class, 'delete'])->whereNumber('id')->name('article.delete');
});



Route::get('/login', function () {
    return view('auth.login');
});

Route::get('/registration', function () {
    return view('auth.register');
});

