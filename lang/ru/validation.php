<?php

return [
    'required' => 'Поле :attribute обязательно для заполнения.',
    'max' => [
        'array' => 'Поле :attribute не должно содержать более :max элементов.',
        'file' => 'Поле :attribute не должно превышать :max количество килобайт.',
        'numeric' => 'Поле :attribute не должно превышать :max.',
        'string' => 'Поле :attribute не должно превышать :max количество символов.',
    ],
    'file' => 'Поле :attribute должно быть файлом.',
    'mimes' => 'Поле :attribute должно быть файлом типа: :values.',

    'attributes' => [
        'title' => 'название',
        'text' => 'текст',
        'password' => 'пароль',
        'image' => 'изображение',
        'publication_status' => 'статус публикации'
    ]
];
